#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <vector>
#include <optional>
#include <charconv>
#include <arpa/inet.h>


using Database = std::vector<std::string>;
using IPS = std::vector<std::uint64_t>;

IPS ips_ranges;
std::optional<Database> locations;

class CSVRow
{
    public:
        std::string_view operator[](std::size_t index) const
        {
            return std::string_view(&m_line[m_data[index] + 1], m_data[index + 1] -  (m_data[index] + 1));
        }
        std::size_t size() const
        {
            return m_data.size() - 1;
        }
        void readNextRow(std::istream& str)
        {
            std::getline(str, m_line);

            m_data.clear();
            m_data.emplace_back(-1);
            std::string::size_type pos = 0;
            while((pos = m_line.find(',', pos)) != std::string::npos)
            {
                m_data.emplace_back(pos);
                ++pos;
            }
            // This checks for a trailing comma with no data after it.
            pos   = m_line.size();
            m_data.emplace_back(pos);
        }
    private:
        std::string         m_line;
        std::vector<int>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}   

std::string removeQuotas(std::string str)
{
    if(str.front() == '"')
        str.erase(0, 1);
    if(str.back() == '"')
        str.pop_back();
    return str;
}


Database LoadDatabase(char * path) {
    Database database;
    std::ifstream       file(path);
    uint32_t count = 0;
    std::string::size_type sz;

    CSVRow              row;
    while(file >> row)
    {
        database.push_back(removeQuotas(std::string(row[2]))+","+removeQuotas(std::string(row[5])));
        // std::cout << "r0:\t" << row[0] << std::endl;
        // std::cout << "r1:\t" << row[1] << std::endl;
        std::string ip0 = {std::string(row[0])};
        ip0 = ip0.substr(1, ip0.size()-2);
        std::string ip1 = {std::string(row[1])};
        ip1 = ip1.substr(1, ip1.size()-2);
        ips_ranges.push_back(std::stol(ip0));
        ips_ranges.push_back(std::stol(ip1));
        count++;
    }

    // std::cout << "Loaded \t: " << count << " entries" << std::endl;
    // std::cout << "first ip:\t" << ips_ranges[3] << std::endl;

    return database;
}

uint32_t convert( const std::string& ipv4Str )
{
    std::istringstream iss( ipv4Str );
    
    uint32_t ipv4 = 0;
    
    for( uint32_t i = 0; i < 4; ++i ) {
        uint32_t part;
        iss >> part;
        if ( iss.fail() || part > 255 ) {
            throw std::runtime_error( "Invalid IP address - Expected [0, 255]" );
        }
        
        // LSHIFT and OR all parts together with the first part as the MSB
        ipv4 |= part << ( 8 * ( 3 - i ) );

        // Check for delimiter except on last iteration
        if ( i != 3 ) {
            char delimiter;
            iss >> delimiter;
            if ( iss.fail() || delimiter != '.' ) {
                throw std::runtime_error( "Invalid IP address - Expected '.' delimiter" );
            }
        }
    }
    
    return ipv4;
}

std::string PerformLookup(std::string ip) {
    // std::cout << "Searching\t: " << ip << std::endl;
    uint32_t ip_num = convert(ip);
    // std::cout << "Searching\t: " << ip_num << std::endl;
    // std::cout << "on ipranges\t: " << ips_ranges.size() << std::endl;
    for (int i = 0; i < ips_ranges.size(); i+=2) {
        if (ip_num >= ips_ranges[i] && ip_num <= ips_ranges[i+1]) {
            return locations.value()[i/2];
        }
    }
    return "";
}

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "error: usage './sample_app <database.csv>'" << std::endl;
        return EXIT_FAILURE;
    }

    // Indicate that the app is ready
    std::cout << "READY" << std::endl;


    for (std::string cmd; std::getline(std::cin, cmd);) {
        if (cmd.find("LOAD") == 0) {
            locations = LoadDatabase(argv[1]);
            std::cout << "OK" << std::endl;

      } else if (cmd.find("LOOKUP") == 0) {
            if (!locations.has_value()){
                std::cerr << "error: Lookup requested before database was ever loaded" << std::endl;
                return EXIT_FAILURE;
            }
            // std::cout << "last value:\t" << locations.value()[locations.value().size() - 2] << std::endl;
            std::cout << PerformLookup(cmd.substr(7)) << std::endl;

      } else if (cmd.find("EXIT") == 0) {
            std::cout << "OK" << std::endl;
            break;
      } else {
            std::cerr << "error: Unknown command received" << std::endl;
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
