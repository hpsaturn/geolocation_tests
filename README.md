# Geolocation app optmizations

It is a documentation and explanation about possible alternatives of optimization of geolocation app. Please first [download](https://bitbucket.org/hpsaturn/geolocation_tests/downloads/) the code and assets, for replicate all tests.

## Directory structure

I have 5 optimizations on 4 directories and a extra directory for Android build tests. The last one only is for compatibility tests.

The deliveries directories:

- cpp_basic_algorithm 
- cpp_basic_lower_bound
- cpp_sqlite3
- cpp_basic_index (only memory test)
- android 
- geolocation (original directory with the challenge)
- figures


## Compiling and setup the tests

### With Docker

I did a docker file to improve easy replication.

For building the base image, please enter to the root directory and execute:

```bash
docker build -t cmake-tool .
```

For compiling all algorithms in Linux please run:

```bash
./build all
```

For clean all algorithms please run:

```bash
./build clean
```

For compiling each test with special flags, for example run:

```bash
cd cpp_basic_algorithm 
../docker_build bash -c "cd build && cmake -DCMAKE_BUILD_TYPE=Release .. &&  make"
```

### Without Docker

In a Linux or Windows machine please install cmake and sqlite3, for example on Linux:

```bash
apt-get install cmake build-essential sqlite3 libsqlite3-dev
```

For compiling each test, for example run:

```bash
cd cpp_basic_algorithm
cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make
```

# Testing each algorithm test

Here I'm going to explain each algorithm, with the respective test and basic output, with some details about the implementation and requirements for run it. The machine used for that was Linux Machine with octa-core CPU and 16Gb of RAM.

## cpp_basic_algorithm 

This algorithm only use a basic implementation:

- it reads in memory the complete csv database
- it use `string_view` optimization (basic reference to the document)
- it only perform a lineal search of vector of IPs and it extracts the output from the document on memory

Testing:

```bash
cd cpp_basic_algorithm/build
../../geolocation/geolocation_test.py --executable ./geolocation --database ../../geolocation/database.csv
```

Output:

```python
Database loaded Memory usage: 171.1mb Load time: 598ms
OK    1.0.0.0 US Los Angeles Memory usage: 171.1mb Lookup time: 100μs
OK    71.6.28.0 US San Jose Memory usage: 171.1mb Lookup time: 916μs
OK    71.6.28.255 US San Jose Memory usage: 171.1mb Lookup time: 1ms
OK    71.6.29.0 US Concord Memory usage: 171.1mb Lookup time: 707μs
OK    53.103.144.0 DE Stuttgart Memory usage: 171.1mb Lookup time: 591μs
OK    53.255.255.255 DE Stuttgart Memory usage: 171.1mb Lookup time: 608μs
OK    54.0.0.0 US Rahway Memory usage: 171.1mb Lookup time: 532μs
OK    223.255.255.255 AU Brisbane Memory usage: 171.1mb Lookup time: 2ms
OK    5.44.16.0 GB Hastings Memory usage: 171.1mb Lookup time: 111μs
OK    8.24.99.0 US Hastings Memory usage: 171.1mb Lookup time: 129μs
```

Summary:

Good performance but the memory used it's huge. 

## cpp_basic_lower_bound

This algorithm is similar to the last one but it has a improvement using the standard library `std::lower_bound` for improve the speed in each request, but it has the same problem, the memory used.

Testing:

```bash
cd cpp_basic_lower_bound/build
../../geolocation/geolocation_test.py --executable ./geolocation --database ../../geolocation/database.csv
```

Output:

```python
Database loaded Memory usage: 169.57mb Load time: 751ms
OK    1.0.0.0 US Los Angeles Memory usage: 169.57mb Lookup time: 108μs
OK    71.6.28.0 US San Jose Memory usage: 169.57mb Lookup time: 103μs
OK    71.6.28.255 US San Jose Memory usage: 169.57mb Lookup time: 96μs
OK    71.6.29.0 US Concord Memory usage: 169.57mb Lookup time: 101μs
OK    53.103.144.0 DE Stuttgart Memory usage: 169.57mb Lookup time: 95μs
OK    53.255.255.255 DE Stuttgart Memory usage: 169.57mb Lookup time: 101μs
OK    54.0.0.0 US Rahway Memory usage: 169.57mb Lookup time: 35μs
OK    223.255.255.255 AU Brisbane Memory usage: 169.57mb Lookup time: 31μs
OK    5.44.16.0 GB Hastings Memory usage: 169.57mb Lookup time: 31μs
OK    8.24.99.0 US Hastings Memory usage: 169.57mb Lookup time: 32μs
```

## cpp_sqlite3

This implementation try to improve the memory usage, using a third party library, sqlite3. I choose it because in Android and some embedded devices is used by size and performance, but it's possible use a better database. The performance in each query it's not bad, but the memory usage is the best of my implementations.

I added the sqlite3 databases for run the tests, but also I explain here the complete steps from scratch:

### Prerequisites 

For this implementation first I did some changes in the csv file for improve the importation into the database. 

Strip the quotes:

```bash
cat database.csv | sed 's/"//g' > database_without_quotes.csv
```

Sqlite3 setup:

Please install sqlite3 in your system. Enter to the sqlite3, for example running `sqlite3 database.sl3`

Add the schema:

```sql
CREATE TABLE geolocation( ip_start INTEGER NOT NULL, ip_end INTEGER NOT NULL, code TEXT NOT NULL, country TEXT NOT NULL, city TEXT NOT NULL, state TEXT NOT NULL, lat REAL NOT NULL, lon REAL NOT NULL);
``` 

With that we can import the all data, but for improve the performance, first we will create a index:

```sql
CREATE UNIQUE INDEX idx_ipstart ON geolocation (ip_start);
``` 

Add the data:

Into the sqlite3 change to csv mode and import the data and exit for write the new database:

```
.mode csv
.import database_without_quotes.csv geolocation
.exit
```

Testing:

```
cd cpp_sqlite3/build
../../geolocation/geolocation_test.py --executable ./geolocation --database ../database.sl3
```

Output:

Without index:

```python
Database loaded Memory usage: 1.85mb Load time: 175μs
OK    1.0.0.0 US Los Angeles Memory usage: 6.05mb Lookup time: 113ms
OK    71.6.28.0 US San Jose Memory usage: 6.05mb Lookup time: 119ms
OK    71.6.28.255 US San Jose Memory usage: 6.05mb Lookup time: 123ms
OK    71.6.29.0 US Concord Memory usage: 6.05mb Lookup time: 122ms
OK    53.103.144.0 DE Stuttgart Memory usage: 6.05mb Lookup time: 133ms
OK    53.255.255.255 DE Stuttgart Memory usage: 6.05mb Lookup time: 126ms
OK    54.0.0.0 US Rahway Memory usage: 6.05mb Lookup time: 117ms
OK    223.255.255.255 AU Brisbane Memory usage: 6.05mb Lookup time: 159ms
OK    5.44.16.0 GB Hastings Memory usage: 6.05mb Lookup time: 145ms
OK    8.24.99.0 US Hastings Memory usage: 6.05mb Lookup time: 117ms
```

With index:

```python
Database loaded Memory usage: 1.8mb Load time: 175μs
OK    1.0.0.0 US Los Angeles Memory usage: 4.07mb Lookup time: 229μs
OK    71.6.28.0 US San Jose Memory usage: 6.0mb Lookup time: 59ms
OK    71.6.28.255 US San Jose Memory usage: 6.0mb Lookup time: 53ms
OK    71.6.29.0 US Concord Memory usage: 6.0mb Lookup time: 53ms
OK    53.103.144.0 DE Stuttgart Memory usage: 6.0mb Lookup time: 34ms
OK    53.255.255.255 DE Stuttgart Memory usage: 6.0mb Lookup time: 34ms
OK    54.0.0.0 US Rahway Memory usage: 6.0mb Lookup time: 34ms
OK    223.255.255.255 AU Brisbane Memory usage: 6.0mb Lookup time: 230ms
OK    5.44.16.0 GB Hastings Memory usage: 6.0mb Lookup time: 4ms
OK    8.24.99.0 US Hastings Memory usage: 6.0mb Lookup time: 6ms
```

## cpp_basic_index (incomplete)

It's my final implementation. But at the moment to write this guide it's incomplete, only it proof the memory reduction without external database or third party library, and try to use a IPs index in memory, for have the same performance than `lower_bound` but without put the complete csv document in memory, only `byte` position to then use `seek` and extract the final output. The reduction of the memory is about the third part than lower_bound algorithm in my final test.

```bash
../../geolocation/geolocation_test.py --executable ./geolocation --database ../../geolocation/database.csv 
Database loaded Memory usage: 47.62mb Load time: 20ms
```

## Results

![results](figures/table.jpg)  

![results](figures/graph.jpg)  

[Google drive document](https://bit.ly/36NEs6c)

## Android Project

I added a basic Android Project that proof the implementation with the official CMake and NDK tools of Android Studio SDK. 





