FROM gcc:9

ENV APP_VERSION="0.1.0" \
    APP="cmake-tool"

LABEL app.name="${APP}" \
      app.version="${APP_VERSION}" \
      maintainer="Hpsaturn <@hpsaturn>"

RUN set -ex;                                                                      \
    apt-get update;                                                               \
    apt-get install -y cmake sqlite3 libsqlite3-dev;                              \
    apt-get clean;                                                                \
    mkdir -p /workspace/build; \
    chmod a+rwx /workspace/build

USER 1001

WORKDIR /workspace

# ENTRYPOINT ["cmake"]

# CMD ["cmake"]
