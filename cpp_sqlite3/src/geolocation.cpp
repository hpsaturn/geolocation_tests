#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <vector>
#include <optional>
#include <charconv>
#include <iostream>
#include <sqlite3.h>

using Database = std::vector<std::string>;
using IPS = std::vector<std::uint64_t>;

sqlite3 *db;
char *zErrMsg = 0;

class CSVRow
{
    public:
        std::string_view operator[](std::size_t index) const
        {
            return std::string_view(&m_line[m_data[index] + 1], m_data[index + 1] -  (m_data[index] + 1));
        }
        std::size_t size() const
        {
            return m_data.size() - 1;
        }
        void readNextRow(std::istream& str)
        {
            std::getline(str, m_line);

            m_data.clear();
            m_data.emplace_back(-1);
            std::string::size_type pos = 0;
            while((pos = m_line.find(',', pos)) != std::string::npos)
            {
                m_data.emplace_back(pos);
                ++pos;
            }
            // This checks for a trailing comma with no data after it.
            pos   = m_line.size();
            m_data.emplace_back(pos);
        }
    private:
        std::string         m_line;
        std::vector<int>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}   

std::string removeQuotas(std::string str)
{
    if(str.front() == '"')
        str.erase(0, 1);
    if(str.back() == '"')
        str.pop_back();
    return str;
}


bool LoadDatabase(char * path) {
    Database database;
    // std::ifstream       file(path);

    int rc;

    rc = sqlite3_open(path, &db);
    if (rc) {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
        return false;
    }
    // rc = sqlite3_exec(db, argv[2], callback, 0, &zErrMsg);
    // if (rc != SQLITE_OK) {
    //     fprintf(stderr, "SQL error: %s\n", zErrMsg);
    //     sqlite3_free(zErrMsg);
    // }
    // sqlite3_close(db);

    // std::cout << "Loaded \t: " << count << " entries" << std::endl;
    // std::cout << "first ip:\t" << ips_ranges[3] << std::endl;

    return true;
}

uint32_t convert( const std::string& ipv4Str )
{
    std::istringstream iss( ipv4Str );
    
    uint32_t ipv4 = 0;
    
    for( uint32_t i = 0; i < 4; ++i ) {
        uint32_t part;
        iss >> part;
        if ( iss.fail() || part > 255 ) {
            throw std::runtime_error( "Invalid IP address - Expected [0, 255]" );
        }
        
        // LSHIFT and OR all parts together with the first part as the MSB
        ipv4 |= part << ( 8 * ( 3 - i ) );

        // Check for delimiter except on last iteration
        if ( i != 3 ) {
            char delimiter;
            iss >> delimiter;
            if ( iss.fail() || delimiter != '.' ) {
                throw std::runtime_error( "Invalid IP address - Expected '.' delimiter" );
            }
        }
    }
    
    return ipv4;
}

static int callback(void* NotUsed, int argc, char** argv, char** azColName) {
    // int i;
    // std::string out;
    printf("%s,%s\n", argv[0] ? argv[0] : "NULL", argv[1] ? argv[1] : "NULL");
    // for (i = 0; i < argc; i++) {
    // }
    // std::cout << out  << std::endl;
    return 0;
}

std::string toString(uint32_t &i) {
    std::stringstream ss;
    ss << i;
    return ss.str();
}

std::string PerformLookup(std::string ip) {
    // std::cout << "Searching\t: " << ip << std::endl;
    uint32_t ip_num = convert(ip);
    std::string ipstr = toString(ip_num);
    std::string query = "select code,state from geolocation where ip_start<=" + ipstr;
    query += " and ip_end>=" + ipstr;
    // std::cout << "query\t: " << query.c_str() << std::endl;
    int rc = sqlite3_exec(db,query.c_str(), callback, 0, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    return "";
}

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "error: usage './sample_app <database.sl3>'" << std::endl;
        return EXIT_FAILURE;
    }

    bool loadeddb = false;
    // Indicate that the app is ready
    std::cout << "READY" << std::endl;

    for (std::string cmd; std::getline(std::cin, cmd);) {
        if (cmd.find("LOAD") == 0) {
            loadeddb = LoadDatabase(argv[1]);
            if(loadeddb) std::cout << "OK" << std::endl;
            else std::cout << "ERROR" << std::endl;

      } else if (cmd.find("LOOKUP") == 0) {
            if (!loadeddb){
                std::cerr << "error: Lookup requested before database was ever loaded" << std::endl;
                return EXIT_FAILURE;
            }
            // std::cout << "last value:\t" << locations.value()[locations.value().size() - 2] << std::endl;
            PerformLookup(cmd.substr(7));

      } else if (cmd.find("EXIT") == 0) {
            std::cout << "OK" << std::endl;
            break;
      } else {
            std::cerr << "error: Unknown command received" << std::endl;
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
