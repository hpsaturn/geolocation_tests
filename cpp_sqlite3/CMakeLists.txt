# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

# set the project name
project(geolocation_app)

set(CMAKE_CXX_STANDARD 17)

find_path(SQLite3_INCLUDE_DIR NAMES sqlite3.h)
find_library(SQLite3_LIB_DIR sqlite3)

message(STATUS "sqlite3 header ${SQLite3_INCLUDE_DIR}")
message(STATUS "sqlite3 lib ${SQLite3_LIB_DIR}")
add_executable(geolocation src/geolocation.cpp)
target_link_libraries(geolocation sqlite3)

